
/*
 *Exercise PALINDROME
 * Application qui vérifie si une chaine de caractères peut se lire 
 * aussi bien dans un sens que l'en l'autre.
 * William SLOWIKOWSKI
 */


package palindrome;

import java.util.Scanner;


public class Palindrome {

    public static void main(String[] args) {
        
        //je crée une variable de type string nommée motSaisi
        String motSaisi = "";
        //un 
        Scanner saisie = new Scanner(System.in);

        System.out.println("Veuillez saisir un mot ou une chaine de caracteres : ");
        motSaisi = saisie.nextLine();

        if (fonctionPalindrome(motSaisi))
            /*si le resultat de fonctionPalindrome est vrai alors j'écris 
            la phrase suivante avec dans la variable motSaisi, 
            le mot qui a été récupéré
            */ 
            System.out.println("La chaine de caractères " + "- " + motSaisi + " -" + " est un palindrome \n");
        else
            /*sinon si le resultat de fonctionPalindrome est faux alors j'écris 
            la phrase suivante avec dans la variable motSaisi, 
            le mot qui a été récupéré
            */
            System.out.println("Désolé la chaine de caractères " + "- " + motSaisi + " -" + " n'est pas  un palindrome ! \n");
            //Phrase optionnelle qui indique que le programme est fini
            System.out.println("Fin du programme");

    }   

   
    
    /*
    Je crée une fonction qui se nomme fonctionPalindrome 
    avec en paramtètre une variable de type string et nommee motEntier
    */
    public static boolean fonctionPalindrome(String motEntier){
        //j'initialise une variable de type entier nommée i
        int i = 0;
        //j'initialise une variable de type entier nommée longueur
        int longueur = motEntier.length()-1;
        //j'initialise une variable de type Booléen nommée egal
        boolean egal = true;
        /*
        je teste le premier caractere de motEntier avec le dernier, si les 
        deux caractères sont égaux la boucle continue et teste le caractère
        suivant (i+1 aveclongueur-i+1) jusqu'a ce que i soit egal a la longueur
        divisée par 2.
        */
        while(i<longueur/2 && egal){
            
            egal = motEntier.charAt(i)== motEntier.charAt(longueur-i);
            i++;
        }
        // je renvoie egal donc true(vrai) sinon ca retourne false (faux)
        return egal;
    }
}
    
